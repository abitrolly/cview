module gitlab.com/tslocum/cview

go 1.12

require (
	github.com/gdamore/tcell v1.3.1-0.20200608133353-cb1e5d6fa606
	github.com/lucasb-eyer/go-colorful v1.0.3
	github.com/mattn/go-runewidth v0.0.9
	github.com/rivo/uniseg v0.1.0
	gitlab.com/tslocum/cbind v0.1.1
	golang.org/x/sys v0.0.0-20200610111108-226ff32320da // indirect
)
